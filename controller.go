package main

import (
	"github.com/martini-contrib/render"
	"log"
	"github.com/martini-contrib/sessions"
	"github.com/go-martini/martini"
	"strconv"
	"github.com/nu7hatch/gouuid"
	"net/smtp"
	"encoding/base64"
	"fmt"
)

const current_user_id = "current_user_id"
var anonymous_user = UserModel{Id:0, FirstName:"Anonymous", Email:"No email"}

func Login(session sessions.Session, loginUser LoginModel, r render.Render) {
	log.Println("binded user to login", loginUser)

	id, _ := dbmap.SelectInt("select count(*) from users where Confirmed = 0 and lower(email) = lower($1)", loginUser.Email)
	if id > 0 {
		log.Println("User not activated")
		r.JSON(400, Message{"Not active user"})
		return
	}

	user := UserModel{}
	err := dbmap.SelectOne(&user, "select * from users where Confirmed = 1 and lower(email) = lower($1) and passwordMD5 = $2",
		loginUser.Email, getMD5(loginUser.Password))
	if err != nil {
		log.Println("Can't get user for login action", err)
		r.JSON(500, Message{"DB problem, please contact with admins"})
		return
	} else {
		session.Set(current_user_id, user.Id)
		r.JSON(200, user)
		return
	}
}

func Logout(s sessions.Session, r render.Render) {
	s.Delete(current_user_id)
	log.Println("Deleted key from session store")

	r.Redirect(toApiResources(root_url), 302)
}

func Registration(reg RegistrationModel, r render.Render) {
	EscapeModelForHTML(&reg)

	id, err := dbmap.SelectInt("select count(*) from users where lower(email) = lower($1)", reg.Email)
	if err != nil {
		log.Println("Cant check current user\n", err)
		r.Text(500, err.Error())
		return
	}

	if (id > 0) {
		log.Println("User with email " + reg.Email + " already exist", id)
		r.JSON(400, Message{"User already exist"})
		return
	}

	user := UserModel{FirstName:reg.FirstName, LastName:reg.LastName, Email:reg.Email, PasswordMD5:getMD5(reg.Password)}
	dbmap.Insert(&user)
	log.Println("User created for email " + user.Email + " with id " + intToString(user.Id))

	u := generateUUID()
	token := TokenModel{ProfileId:user.Id, Token:u.String()}
	dbmap.Insert(&token)
	log.Println("Generated token: " + token.Token)

	sendMail(token.Token, user.Email)
	r.Status(201)
}

func Activate(s sessions.Session, params martini.Params, r render.Render) {
	token := params["token"]
	log.Println("Try to activate user for token: " + token)

	id, err := dbmap.SelectInt("select profileid from tokens where token = $1", token)
	if err != nil {
		r.JSON(500, Message{err.Error()})
		return
	}
	if id == 0 {
		r.JSON(404, Message{"No tokens"})
		return
	}

	_, err = dbmap.Exec("update users set Confirmed = 1 where Id = $1",id)
	defer dbmap.Exec("delete from tokens where token = $1", token)
	if err != nil {
		r.JSON(500, Message{"Can't activate user\n " + err.Error()})
		return
	}

	s.Set(current_user_id, id)
	r.Redirect(toApiResources(root_url))
}

func GetUsersList(r render.Render) {
	var users []UserModel
	_, err := dbmap.Select(&users, "select * from users order by Id")
	if err != nil {
		log.Println("Can't select all users\n", err)
		r.JSON(500, err)
	} else {
		r.JSON(200, users)
	}

}

func GetUserById(s sessions.Session, params martini.Params, r render.Render) {
	if "current" == params["id"] {
		getCurrentUser(s, r)
		return
	}
	user, err := findUserById(parseIdFromString(params["id"]))
	if err != nil {
		log.Println("Can't select user by id: " + params["id"] + "\n", err)
		r.Text(404, "Not Found")
	} else {
		r.JSON(200, user)
	}
}

func UpdateUser(s sessions.Session, params martini.Params, prof ProfileModel, r render.Render) {
	EscapeModelForHTML(&prof)

	currentId, ok := s.Get(current_user_id).(int64)
	if !ok {
		log.Println("Anonymous user try to update profile with id: " + intToString(currentId))
		r.JSON(403, Message{"You can update only yourself profile"})
		return
	}

	requestedId := parseIdFromString(params["id"])
	if currentId != requestedId {
		log.Println("User with id: " + intToString(currentId) + " try to update profile with id: " + intToString(requestedId))
		r.JSON(403, Message{"You can update only yourself profile"})
		return
	}

    user, _ := findUserById(currentId)
	history := user.applyWithDiff(&prof)

	dbmap.Update(&user)
	dbmap.Insert(&history)

	r.JSON(200, user)
}

func GetProfileHistory(s sessions.Session, params martini.Params, r render.Render)  {
	currentId, ok := s.Get(current_user_id).(int64)
	if !ok {
		log.Println("Anonymous user try to update profile with id: " + intToString(currentId))
		r.JSON(403, Message{"You can view history only yourself profile"})
		return
	}

	requestedId := parseIdFromString(params["id"])
	if currentId != requestedId {
		log.Println("User with id: " + intToString(currentId) + " try to update profile with id: " + intToString(requestedId))
		r.JSON(403, Message{"You can view history only yourself profile"})
		return
	}
	var h []ProfileHistoryModel
	_, err := dbmap.Select(&h, "select * from history where profileId = $1 order by ChangeTime DESC", currentId)
	if err != nil {
		log.Println(err)
		r.JSON(500, Message{Message:"Can't select user history"})
		return
	}
	r.JSON(200, h)
}

func getCurrentUser(s sessions.Session, r render.Render) {
	id, ok := s.Get(current_user_id).(int64)
	if ok {
		user, err := findUserById(id)
		if (err != nil) {
			r.JSON(200, anonymous_user)
		} else {
			r.JSON(200, user)
		}
	} else {
		r.JSON(200, anonymous_user)
	}
}

func findUserById(id int64) (UserModel, error) {
	var user UserModel
	err := dbmap.SelectOne(&user, "select * from users where id = $1 order by Id", id)

	return user, err
}

func parseIdFromString(s string) int64 {
	id, _ := strconv.ParseInt(s, 10, 64)
	return id
}

func intToString(i int64) string {
	return strconv.FormatInt(i, 10)
}

func generateUUID() *uuid.UUID {
	u, _ := uuid.NewV4()
	for id, _ := dbmap.SelectInt("select count(*) from tokens where token = $1", u.String()); id > 0; {
		u, _ = uuid.NewV4()
		id, _ = dbmap.SelectInt("select count(*) from tokens where token = $1", u.String())
	}

	return u
}

func sendMail(token string, email string) {
	link := "ACTIVATE LINK:  "+ getHost() + app_location + activate_url + "/"+ token
	log.Println(link)

	header := make(map[string]string)
	header["From"] = "junotest@example.org"
	header["To"] = email
	header["Subject"] = "Complete registraion"
	header["MIME-Version"] = "1.0"
	header["Content-Type"] = "text/plain; charset=\"utf-8\""
	header["Content-Transfer-Encoding"] = "base64"

	message := ""
	for k, v := range header {
		message += fmt.Sprintf("%s: %s\r\n", k, v)
	}
	message += "\r\n" + base64.StdEncoding.EncodeToString([]byte(link))

	auth := smtp.PlainAuth(
		"",
		"jjtest201515@gmail.com",
		"qwer1234!",
		"smtp.gmail.com",
	)

	err := smtp.SendMail(
		"smtp.gmail.com:587",
		auth,
		"junotest@example.org",
		[]string{email},
		[]byte(message),
	)
	if err != nil {
		log.Println("Can't send email \n" + err.Error())
	} else {
		log.Println("Email send to " + email + " with body \n" + message)
	}

}

func getHost() string {
	log.Println("Current env: " + martini.Env)
	if martini.Env == martini.Dev {
		return local_host
	} else {
		return docker_host
	}
}