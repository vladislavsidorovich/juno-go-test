package main

import (
	"database/sql"
	"github.com/coopernurse/gorp"
	"github.com/go-martini/martini"
	"github.com/martini-contrib/render"
	"github.com/martini-contrib/sessions"
	_ "github.com/mattn/go-sqlite3"
	"os"
	"log"
	"github.com/martini-contrib/binding"
	"crypto/md5"
	"encoding/hex"
	"github.com/martini-contrib/secure"
	"reflect"
	"html/template"
	"net/http"
)

var dbmap *gorp.DbMap

var docker_host = "https://192.168.99.100:1025"

const (
	APP_PROD_HOST_ENV = "PROD_HOST"

	local_host = "https://localhost:1025"
	app_location = "/juno/"
	service_port = ":8443"
	db_file = "users.bin"

	root_url string = "/"
	login_url string = "/login"
	activate_url string = "/activate"

	users_url string = "/users"
)


func main() {
	initEnv()
	dbmap = initDb()

	sessionStore := sessions.NewCookieStore([]byte("cookie-key"))

	m := martini.Classic()
	m.Use(secure.Secure(secure.Options{
		SSLRedirect: true,
		SSLHost: "localhost:8443",
		FrameDeny: true,
		ContentTypeNosniff: true,
		BrowserXssFilter: true,
	}))
	m.Use(render.Renderer())
	m.Use(sessions.Sessions("gosession", sessionStore))

	m.Get(root_url, func(r render.Render) {
		r.Redirect(toHtmlResources("/signin.html"), 302)
	})

	m.Post(login_url, binding.Bind(LoginModel{}), Login)
	m.Get(activate_url + "/:token", Activate)
	m.Post("/registration", binding.Bind(RegistrationModel{}), Registration)
	m.Get("/logout", Logout)

	m.Get(users_url, GetUsersList)
	m.Get(users_url + "/:id", GetUserById)
	m.Put(users_url + "/:id", binding.Bind(ProfileModel{}), UpdateUser)
	m.Get(users_url + "/:id/history", Auth, GetProfileHistory)

	if err := http.ListenAndServeTLS(service_port, "cert.pem", "key.pem", m); err != nil {
		log.Fatal(err)
	}
}

func EscapeModelForHTML(model interface{}) {
	log.Println("Escape strings..")
	log.Println(model)

	s := reflect.ValueOf(model).Elem()
	for i := 0; i < s.NumField(); i++ {
		f := s.Field(i)
		if (f.Type().Kind() == reflect.String) {
			val, ok := f.Interface().(string)
			if ok {
				f.SetString(template.HTMLEscapeString(val))
			}
		}
	}
}

func Auth(s sessions.Session, r render.Render) {
	id, ok := s.Get(current_user_id).(int64)
	if !ok || id <= 0 {
		log.Println("User not authorized")
		r.JSON(401, Message{Message: "Please authorize"})
	}

	log.Println("User authorized")
}

func initEnv() {
	prodUrl := os.Getenv(APP_PROD_HOST_ENV)
	if len(prodUrl) > 0 {
		docker_host = prodUrl
		log.Println("Docker host set to: " + docker_host)
	} else {
		log.Println("Docker host use default value: " + docker_host)
	}
}

func initDb() *gorp.DbMap {
	_, err := os.Open(db_file)
	if err == nil {
		os.Remove(db_file)
	}

	db, err := sql.Open("sqlite3", db_file)
	if err != nil {
		log.Fatal("Fail to create DB", err)
	}

	dbmap := &gorp.DbMap{Db:db, Dialect: gorp.SqliteDialect{}}

	dbmap.AddTableWithName(UserModel{}, "users").SetKeys(true, "Id")
	dbmap.AddTableWithName(ProfileHistoryModel{}, "history").SetKeys(true, "Id")
	dbmap.AddTableWithName(TokenModel{}, "tokens").SetKeys(true, "Id")
	err = dbmap.CreateTablesIfNotExists()
	if err != nil {
		log.Fatal("Could not create tables", err)
	}

	user := UserModel{Id:0, FirstName:"testuser", Email:"test@test.com", PasswordMD5:getMD5("password"), Confirmed:true}
	user2 := UserModel{Id:0, FirstName:"aaaaaa", LastName:"Last name for second user", Email:"test2@test.com", PasswordMD5:getMD5("password")}

	err = dbmap.Insert(&user, &user2)
	if err != nil {
		log.Fatal("Could not insert test user\n", err)
	}

	return dbmap
}

func getMD5(text string) string {
	hasher := md5.New()
	hasher.Write([]byte(text))
	return hex.EncodeToString(hasher.Sum(nil))
}

func toHtmlResources(path string) string {
	return app_location + "html/" + path
}

func toApiResources(path string) string {
	return app_location + path
}