FROM golang:1.4

# Mail
RUN apt-get update && apt-get install -y ssmtp

#  Nginx
RUN apt-get update && apt-get install -y nginx
RUN mkdir logs
RUN touch logs/api-access.log;
RUN touch logs/api-error.log;

RUN mkdir config
RUN mkdir static

ADD config/* config/
ADD static/ static/

ADD cert.pem cert.pem
ADD key.pem key.pem

#  GO
ADD martini.go martini.go
ADD controller.go controller.go
ADD structs.go structs.go

RUN go get github.com/coopernurse/gorp
RUN go get github.com/go-martini/martini
RUN go get github.com/martini-contrib/binding
RUN go get github.com/martini-contrib/render
RUN go get github.com/martini-contrib/secure
RUN go get github.com/martini-contrib/sessions
RUN go get github.com/mattn/go-sqlite3
RUN go get github.com/nu7hatch/gouuid

EXPOSE 1025
EXPOSE 8443

ENV MARTINI_ENV=production
ENV PROD_HOST=https://192.168.99.100:1025

ENTRYPOINT sh -c 'nginx -c config/nginx.conf -p /go/ && go run martini.go controller.go structs.go'