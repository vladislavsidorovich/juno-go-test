jQuery.fn.exists = function () {
    return this.length > 0;
};

const users_html = '/juno/html/users.html';
const activate_html = '/juno/html/activate.html';

$(document).ready(function () {
    var closeBtn = $('.close');
    closeBtn.click(function () {
        closeBtn.parent().hide();
    });

    var updateUsers = function () {
        $.get(
            '/juno/users', {},
            onListComplete
        )
    };

    $('#siginBtn').click(function () {
        var form = $('#signinForm');
        var serialize = getDataJson(form);
        var json = JSON.stringify(serialize);

        console.log("signin form");
        console.log(json);

        $.ajax('/juno/login', {
            data: json,
            contentType: 'application/json',
            type: 'POST',
            success: function (data) {
                console.log("success login");
                console.log(data);
                location.href = users_html;
            },
            error: function (data) {
                console.log("can't login");
                console.log(data);
                if (data.status == 400) {
                    $('#loginError').hide();

                    $('#notActiveMessage').html(data.responseJSON.message);
                    $('#loginNotActive').show();
                } else {
                    $('#loginNotActive').hide();
                    $('#loginError').show();
                }
            }
        });

    });
    $('#signupBtn').click(function () {
        $('#formError').hide();
        $('#emailError').hide();
        $('#passwordError').hide();
        $('#serverError').hide();

        var form = $('#signupForm');
        if (isBlank(form.find('input[name="firstName"]').val()) ||
            isBlank(form.find('input[name="email"]').val()) ||
            isBlank(form.find('input[name="password"]').val()) ||
            isBlank(form.find('input[name="passwordRepeat"]').val())) {
            $('#formError').show();
            return;
        }

        if (!isEmail(form.find('input[name="email"]').val())) {
            $('#emailError').show();
            return;
        }
        if (form.find('input[name="password"]').val() != form.find('input[name="passwordRepeat"]').val()) {
            $('#passwordError').show();
            return;
        }
        var serialize = getDataJson(form);
        var json = JSON.stringify(serialize);

        $.ajax('/juno/registration', {
            data: json,
            contentType: 'application/json',
            type: 'POST',
            success: function (data) {
                console.log("registration success");
                console.log(data);
                location.href = activate_html
            },
            error: function (data) {
                console.log(data);
                $('#errorMessage').html(data.responseJSON.message);
                $('#serverError').show();

            }
        });
    });

    var rowTemplate = '<tr> <td>{{id}}</td> <td>{{firstName}}</td> <td>{{lastName}}</td><td>{{email}}</td> </tr>';
    var profileTemplate = '<a href="/juno/html/profile.html#{{id}}" class="btn-link">{{id}}</a>';

    function onListComplete(data) {
        var tbl = $('#usersList tbody');

        var json = data;
        if (!json) {
            tbl.html('');
            return;
        }

        var tbody = '';
        for (var i in json) {
            var profileLink = profileTemplate.replace(/{{id}}/, json[i].id);
            profileLink = profileLink.replace(/{{id}}/, json[i].id);

            var row = rowTemplate.replace(/{{id}}/, profileLink);
            row = row.replace(/{{firstName}}/, json[i].firstName);
            row = row.replace(/{{lastName}}/, json[i].lastName);
            row = row.replace(/{{email}}/, json[i].email);

            console.log(row);
            tbody += row;
        }

        tbl.html(tbody);
    }

    function getDataJson(form) {
        var o = {};
        var a = form.serializeArray();
        $.each(a, function () {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });

        return o;
    }

    function isBlank(str) {
        return (!str || /^\s*$/.test(str));
    }

    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }

    if ($('#usersList').exists()) {
        updateUsers();
    }

    var getCurrentUser = function (func) {
        $.ajax('/juno/users/current', {
            contentType: 'application/json',
            type: 'GET',
            success: func
        })
    };

    if ($('#signinForm').exists()) {
        getCurrentUser(function (currentUser) {
            console.log(currentUser);
            if (currentUser.id > 0) { //not anonymous
                location.href = users_html;
            }
        });
    }


    var profileForm = ' \
        <label class="input-block-level" name="firstName">{{firstName}}</label>\
        <label class="input-block-level" name="lastName" >{{lastName}}</label>\
        <label class="input-block-level" name="email">{{email}}</label>';
    var editProfileForm = '\
    <input type="text" class="input-block-level" required="required" name="firstName" placeholder="First name" value="{{firstName}}">\
    <input type="text" class="input-block-level" name="lastName" placeholder="Last name" value="{{lastName}}">\
    <input type="email" disabled="disabled" class="input-block-level" required="required" name="email" placeholder="Email address" value="{{email}}">\
    <label>Some description</label>\
    <textarea rows="3" name="description" placeholder="Some description">{{description}}</textarea>\
        \
        ';

    var historyLink = '<a id="historyLink" href="/juno/html/history.html#{{id}}" class="btn-link">History</a>';

    if ($('#profileForm').exists()) {
        var profileId = location.hash.split('#')[1];
        if (profileId) {
            console.log(profileId);

            $('#saveBtn').click(function () {

                var serialize = getDataJson($('#profileForm'));
                var json = JSON.stringify(serialize);

                $.ajax('/juno/users/' + profileId, {
                    data: json,
                    contentType: 'application/json',
                    type: 'PUT',
                    success: function () {
                        $('.alert-success').show();
                    },
                    error: function (data) {
                        console.log(data);
                    }
                });
            });

            getCurrentUser(function (currentUser) {
                    console.log(currentUser);
                    $.ajax('/juno/users/' + profileId, {
                        contentType: 'application/json',
                        type: 'GET',
                        success: function (profileUser) {
                            console.log(profileUser);

                            $('#historyPlace').html(historyLink.replace(/{{id}}/, profileUser.id));

                            const selfProfile = currentUser.id == profileUser.id;
                            var profileTemplate = selfProfile ? editProfileForm : profileForm;
                            var html = profileTemplate.replace(/{{firstName}}/, profileUser.firstName);
                            html = html.replace(/{{lastName}}/, profileUser.lastName);
                            html = html.replace(/{{email}}/, profileUser.email);
                            html = html.replace(/{{description}}/, profileUser.description);

                            $('#profileBody').html(html);
                            if (selfProfile) {
                                $('#saveBtn').show();
                            }
                        },
                        error: function (data) {
                            console.log(data);
                            $('#profileBody').html('<div class="well well-large"><h3>User not found</h3></div>');
                        }
                    });
                }
            );
        } else {
            console.log('No profile id specified');
            $('#profileBody').html('<div class="well well-large"><h3>No user selected</h3></div>');
        }

    }
});
