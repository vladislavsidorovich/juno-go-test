$(document).ready(function () {
    var historyTable = $('#historyTable');
    var profileId = location.hash.split('#')[1];
    if (!profileId) {
        console.log('No profile id specified');
        historyTable.html('<div class="well well-large"><h3>No user selected</h3></div>');

        return;
    }

    function endsWith(str, suffix) {
        return str.indexOf(suffix, str.length - suffix.length) !== -1;
    }

    var historyRow = '\
    <tr>\
        <td>{{new}}</td>\
        <td>{{old}}</td>\
        <td>{{time}}</td>\
    </tr>\
    ';
    //var changeRow = '<strong>{{name}}</strong><div class="well well-small">{{value}}</div>';
    var changeRow = '<strong>{{name}}</strong><blockquote><p>{{value}}</p></blockquote>';

    $.get('/juno/users/' + profileId + '/history')
        .success(function (history) {
            var tblBody = historyTable.find('tbody');
            if (!history) {
                tblBody.html('');
                return;
            }

            var tbody = '';
            for (var i in history) {
                var row = historyRow.replace(/{{time}}/, history[i].time);

                var newTd = '';
                var oldTd = '';
                $.each(history[i], function (key, val) {
                    if (val && (endsWith(key, 'New'))) {
                        newTd += changeRow.replace(/{{name}}/, humanize(key)).replace(/{{value}}/, val);
                    }
                    if (val && (endsWith(key, 'Old'))) {
                        oldTd += changeRow.replace(/{{name}}/, humanize(key)).replace(/{{value}}/, val);
                    }
                });

                row = row.replace(/{{new}}/, newTd);
                row = row.replace(/{{old}}/, oldTd);

                console.log(row);

                tbody += row;
            }

            tblBody.html(tbody);
        })
        .error(function (data) {
            console.log(data);
            if (401 == data.status) {
                location.href = '/juno/html/signin.html';
            }
        });


    function humanize(str) {
        if (!str) {
            return '';
        }
        if (endsWith(str, 'Old')) {
            str = str.substr(0, str.length - 3);
        } else if (endsWith(str, 'New')) {
            str = str.substr(0, str.length - 3);
        }
        return str.charAt(0).toUpperCase() + str.substring(1);
    }

});