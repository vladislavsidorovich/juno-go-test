# How to run
* download code
* install nginx and go
* edit **run_nginx.sh** specify your location for **c** and **p** params

```
./run_nginx.sh && go run martini.go controller.go structs.go 
```
**or**

* pull https://hub.docker.com/r/vladislav/juno-go/ 
*NOTE: by default app will look at VB ip https://192.168.99.100:1025 for built activation link, you can customise it by env var PROD_HOST=https://....:1025)*
* by default app runs with martini.Prod vars, you can change it to martini.Dev in this case 'localhost' will used for built activation links (PROD_HOST will ignored)
```
docker run -p 8080:8443 -p 1025:1025 juno-go
or
docker run -p 8080:8443 -p 1025:1025 -e "PROD_HOST=https://192.168.99.100:1025" juno-go
or
docker run -p 8080:8443 -p 1025:1025 -e "MARTINI_ENV=development" juno-go
```

# REST Api docs
* http://docs.junogo.apiary.io/#


# JunoLab test requirements


## Implementation requrements

- <any> *nix
- docker
- golang
- <any> DB
- API docs


## Features

common:
- registration (with confirmation token)
- registration confirmation by token

anonymous user features:
- show list of registered users
- show any particular user's profile

authenticated user features:
- update my profile info {full name / addr / phone / etc.}
- show updates history


## Security requirements

- external access: static container, APP container
- DB container should be isolated from the outer world
- TLS
- XSS protection (1-2 cases)


## Infrastructure

```
     +-----+      +----+
---> | APP | <--> | DB |
     +-----+      +----+

     +--------+
---> | Static |
     +--------+
```

Static container:
- serves css/js/static elements

App container:
- runs appserver's binary
- goes to DB container for data

DB container:
- runs DB server (choose any)