package main

import "time"

type Hello struct {
	Hello string `json:"hello"`
}

type UserModel struct {
	Id          int64  `json:"id" db:"id"`
	FirstName   string `json:"firstName"`
	LastName    string `json:"lastName"`

	Email       string `json:"email"`

	Description string `json:"description"`
	PasswordMD5 string `json:"-"`

	Confirmed   bool   `json:"-"`
}

func (u *UserModel) applyWithDiff(prof *ProfileModel) ProfileHistoryModel {
	history := ProfileHistoryModel{ProfileId:u.Id, ChangeTime:time.Now()}
	if u.FirstName != prof.FirstName {
		history.FirstNameOld = u.FirstName
		history.FirstNameNew = prof.FirstName

		u.FirstName = prof.FirstName
	}
	if u.LastName != prof.LastName {
		history.LastNameOld = u.LastName
		history.LastNameNew = prof.LastName

		u.LastName = prof.LastName
	}
	if u.Description != prof.Description {
		history.DescriptionOld = u.Description
		history.DescriptionNew = prof.Description

		u.Description = prof.Description
	}

	return history
}

type LoginModel struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type RegistrationModel struct {
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`

	Email     string `json:"email"`
	Password  string `json:"password"`
}

type TokenModel struct {
	Id        int64

	ProfileId int64
	Token     string
}

type ProfileModel struct {
	FirstName   string `json:"firstName"`
	LastName    string `json:"lastName"`

	Description string `json:"description"`
}

type ProfileHistoryModel struct {
	Id             int64
	ProfileId      int64  `json:"profileId"`
	FirstNameOld   string `json:"firstNameOld"`
	FirstNameNew   string `json:"firstNameNew"`
	LastNameOld    string `json:"lastNameOld"`
	LastNameNew    string `json:"lastNameNew"`

	DescriptionOld string `json:"descriptionOld"`
	DescriptionNew string `json:"descriptionNew"`

	ChangeTime     time.Time   `json:"time", db:"changeTime"`
}

type Message struct {
	Message string `json:"message"`
}